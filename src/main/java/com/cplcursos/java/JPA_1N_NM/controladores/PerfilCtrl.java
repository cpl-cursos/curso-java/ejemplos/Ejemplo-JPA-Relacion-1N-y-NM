package com.cplcursos.java.JPA_1N_NM.controladores;

import com.cplcursos.java.JPA_1N_NM.modelos.Perfil;
import com.cplcursos.java.JPA_1N_NM.servicios.ifxPerfilSrvc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class PerfilCtrl {
    @Autowired
    private ifxPerfilSrvc perfsrvc;

    @GetMapping("/listaperfiles")
    public List<Perfil> listaPerfiles() {
        return perfsrvc.listaperfiles();
    }
}
