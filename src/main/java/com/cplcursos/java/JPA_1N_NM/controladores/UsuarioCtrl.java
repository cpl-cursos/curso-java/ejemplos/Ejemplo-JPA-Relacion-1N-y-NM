package com.cplcursos.java.JPA_1N_NM.controladores;

import com.cplcursos.java.JPA_1N_NM.modelos.Usuario;
import com.cplcursos.java.JPA_1N_NM.servicios.UsuarioSrvcImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UsuarioCtrl {

    @Autowired
    private UsuarioSrvcImpl usuSrvc;

    @GetMapping("/listausus")
    public List<Usuario> listaUsuarios() {
        return usuSrvc.listaUsuarios();
    }
}
