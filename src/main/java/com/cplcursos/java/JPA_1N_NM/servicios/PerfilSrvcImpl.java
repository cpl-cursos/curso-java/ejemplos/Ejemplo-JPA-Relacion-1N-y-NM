package com.cplcursos.java.JPA_1N_NM.servicios;

import com.cplcursos.java.JPA_1N_NM.DAOs.ifxPerfilDAO;
import com.cplcursos.java.JPA_1N_NM.modelos.Perfil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PerfilSrvcImpl implements ifxPerfilSrvc{

    @Autowired
    private ifxPerfilDAO perfDAO;


    @Override
    public List<Perfil> listaperfiles() {
        return perfDAO.findAll();
    }
}
