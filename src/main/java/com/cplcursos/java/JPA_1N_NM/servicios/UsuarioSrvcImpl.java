package com.cplcursos.java.JPA_1N_NM.servicios;

import com.cplcursos.java.JPA_1N_NM.DAOs.ifxUsuarioDAO;
import com.cplcursos.java.JPA_1N_NM.modelos.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioSrvcImpl implements ifxUsuarioSrvc{
    @Autowired
    private ifxUsuarioDAO usuDAO;

    @Override
    public List<Usuario> listaUsuarios() {
        return usuDAO.findAll();
    }
}
