package com.cplcursos.java.JPA_1N_NM.DAOs;

import com.cplcursos.java.JPA_1N_NM.modelos.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ifxUsuarioDAO extends JpaRepository<Usuario, Long> {
}
