package com.cplcursos.java.JPA_1N_NM.DAOs;

import com.cplcursos.java.JPA_1N_NM.modelos.Perfil;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ifxPerfilDAO extends JpaRepository<Perfil, Long> {
}
