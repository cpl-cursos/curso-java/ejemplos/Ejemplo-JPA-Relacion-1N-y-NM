package com.cplcursos.java.JPA_1N_NM.modelos;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="Usuarios")
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(columnDefinition = "VARCHAR(45)")
    private String nombre;
    @Column(columnDefinition = "VARCHAR(75)")
    private String apellidos;
    @Column(columnDefinition = "VARCHAR(45)")
    private String usuario;
    @Column(columnDefinition = "VARCHAR(100)")
    private String clave;
}
