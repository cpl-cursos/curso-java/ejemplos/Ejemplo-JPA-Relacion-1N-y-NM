package com.cplcursos.java.JPA_1N_NM;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jpa1NNmApplication {

	public static void main(String[] args) {
		SpringApplication.run(Jpa1NNmApplication.class, args);
	}

}
